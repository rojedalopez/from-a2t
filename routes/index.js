var express = require('express');
var router = express.Router();
const Frases = require('../models/frases');

router.get('/frases-json', function(req, res, next) {
  //busco todas las grases guardadas
  Frases.find( function(err, frases) {
      if(err){console.log(err);}
      res.send(frases);
    });
});

router.get('/palabras-json', function(req, res, next) {
  //cuento la cantidad de veces que aparecen las palabras en los textos
  Frases
  .aggregate([{$project:{"palabras":1,_id:0}},{$unwind:"$palabras"},{$group:{_id:"$palabras",cantidad:{$sum:1}}}, { $sort : { cantidad : -1}}  ])
  .exec(function(err, palabras) {
      if(err){console.log(err);}
      res.send(palabras);
    });
});

router.get('/', function(req, res, next) {
  //renderizo la nube de palabras
  Frases
  .aggregate([{$project:{"palabras":1,_id:0}},{$unwind:"$palabras"},{$group:{_id:"$palabras",cantidad:{$sum:1}}}, { $sort : { cantidad : -1}}  ])
  .exec(function(err, palabras) {
      if(err){console.log(err);}
      res.render('index', { title: 'Audio-to-Text', palabras: palabras });
    });
});

module.exports = router;
