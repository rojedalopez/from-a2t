var mongoose = require('../config/connections');  
var Schema = mongoose.Schema;

// create a schema
//db.frasess.update({ $set:{"sldo_fvor" : 0} }, {multi:true})
var frasesSchema = new Schema({
  texto: { type: String, required: true },
  url: String,
  palabras: [{ type: String }],
  created_at: Date
}, {versionKey: false});

frasesSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});


// the schema is useless so far
// we need to create a model using it
var frases = mongoose.model('Frases', frasesSchema);

// make this available to our frasess in our Node applications
module.exports = frases;